#language: pt

Funcionalidade: Conversa via Microfone

Como usuário do aplicativo WhatsApp 
Eu quero utilizar a função Microfone 
De modo que eu saiba Gravar, Enviar e Cancelar áudio

Contexto:
    Dado que eu entre no Whatsapp
    E eu acesso uma conversa

Cenario: Enviar áudio

    Quando pressiono o ícone referente ao Microfone
    E gravo um áudio
    E solto para enviar
    Então conversa via áudio é enviada

Cenario: Cancelar áudio

    Quando pressiono o ícone referente ao Microfone
    E gravo um áudio 
    E deslizo para lateral esquerda
    Então conversa via áudio é cancelada