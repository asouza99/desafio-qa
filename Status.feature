#language: pt

Funcionalidade: Status
Como usuário do aplicativo WhatsApp 
Eu quero utilizar a função Status 
De modo que eu possa Atualizar, Selecionar, Apagar e Limpar Status

Contexto:
    Dado que eu entro no Whatsapp
    E eu acesso Ajustes
    E clico em Perfil

Cenario: Criar/Atualizar Status

    Quando toco em Status Atual
    E preencho status atual com "."
    E salvo
    Então status atual "." é criado

Cenario: Selecionar Novo Status

    Quando seleciono nova opção de status
    Então novo status é atualizado

Cenario: Limpar Status

    Quando clico em Limpar Status
    Então Status Atual é atualizado para Sem Status

Cenario: Apagar Status "."

    Quando pressiono Editar
    E seleciono o status "."
    E clico em apagar
    Então status "." é apagado

Cenario: Apagar Todos os Status

    Quando pressiono Editar
    E clico em Apagar Tudo
    Então todos os Status são apagados